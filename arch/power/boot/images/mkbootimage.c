#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>


#include "bootrec.h"

boot_record_t bootrec;
config_record_t confrec;

static void build_records(int img_len)
{
	memset(&bootrec, 0, sizeof bootrec);
	memset(&confrec, 0, sizeof confrec);

	bootrec.ipl_record = htonl(IPLRECID);
	bootrec.formatted_cap = htonl(2880);
	bootrec.floppy_last_head = 1;
	bootrec.floppy_last_sec = 18;
	bootrec.bootcode_len = htonl((img_len+511)/512);
//	bootrec.bootcode_off = htonl(0x200);
	bootrec.bootcode_off = htonl(0);
	bootrec.bootpart_start = htonl(2);
	bootrec.bootprg_start = htonl(2);
	bootrec.bootpart_len = htonl((img_len+511)/512);
	bootrec.boot_load_addr = htonl(0);
	bootrec.boot_frag = 1;
	bootrec.boot_emul = 2;
	bootrec.servcode_len = bootrec.bootcode_len;
	bootrec.servcode_off = bootrec.bootcode_off;
	bootrec.servpart_start = bootrec.bootpart_start;
	bootrec.servprg_start = bootrec.bootprg_start;
	bootrec.servpart_len = bootrec.bootpart_len;
	bootrec.serv_load_addr = bootrec.boot_load_addr;
	bootrec.serv_frag = bootrec.boot_frag;
	bootrec.serv_emul = bootrec.boot_emul;

	confrec.conf_rec = htonl(CONFRECID);
	confrec.formatted_cap = htonl(2880);
	confrec.sector_size = 2;
	confrec.last_cyl = htons(79);
	confrec.last_head = 1;
	confrec.last_sec = 18;
}

int main(int argc, char **argv)
{
	FILE *fb;

	build_records(1450117);

	fb = fopen("uImage", "wb");
	fwrite(&bootrec, sizeof bootrec, 1, fb);
	fwrite(&confrec, sizeof confrec, 1, fb);
	fseek(fb, 0x600, SEEK_SET);
	fclose(fb);
	return 0;
}
