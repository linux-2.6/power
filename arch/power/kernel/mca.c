#include <linux/module.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/mca.h>
#include <linux/kprobes.h>
#include <asm/system.h>
#include <asm/io.h>
#include <linux/proc_fs.h>
#include <linux/mman.h>
#include <linux/mm.h>
#include <linux/pagemap.h>
#include <linux/ioport.h>
#include <asm/uaccess.h>
#include <linux/init.h>
//include <asm/arch_hooks.h>

int MCA_bus = 1;	// We assume per default to have one.
			// On the x86 platform this is set by setup.c when
			// an MCA bus is detected.
EXPORT_SYMBOL(MCA_bus);

/* Build the status info for the adapter */

void mca_show_iocc_config(void)
{
	unsigned long config = *(unsigned long *)(0xB0400000+0x10);

	printk("--------- MCA IOCC CONFIG --------\n");
	printk("master %s\n", (config & 0x80000000) ? "enabled" : "disabled");
	printk("burst control %lu\n", (config >> 28) & 3);
	printk("parity check: %s\n", (config & 0x08000000) ? "disabled" : "enabled");
	printk("streaming: %s\n", (config & 0x04000000) ? "disabled" : "enabled");
	printk("refresh: %lu\n", (config >> 24) & 3);
	printk("RAM: %lu kB\n", 128UL << ((config >> 20) & 7));
	printk("arb time: %lu ns\n", (((config >> 16) & 0xF)+1)*100);
	printk("dual buffer: %ssupported\n", (config & 0x80) ? "" : "not ");
	printk("slave with TCWs: %s\n", (config & 0x40) ? "yes" : "no");
	printk("buffer/coherency: %lu\n", (config >> 6) & 1);
	printk("number of DMA slave channels: %lu\n", config & 0xf);
	printk("----------------------------------\n");
}

static void mca_configure_adapter_status(struct mca_device *mca_dev) {
	mca_dev->status = MCA_ADAPTER_NONE;

	mca_dev->pos_id = mca_dev->pos[0]
		+ (mca_dev->pos[1] << 8);

	if(!mca_dev->pos_id && mca_dev->slot < MCA_MAX_SLOT_NR) {

		/* id = 0x0000 usually indicates hardware failure,
		 * however, ZP Gu (zpg@castle.net> reports that his 9556
		 * has 0x0000 as id and everything still works. There
		 * also seem to be an adapter with id = 0x0000; the
		 * NCR Parallel Bus Memory Card. Until this is confirmed,
		 * however, this code will stay.
		 */

		mca_dev->status = MCA_ADAPTER_ERROR;

		return;
	} else if(mca_dev->pos_id != 0xffff) {

		/* 0xffff usually indicates that there's no adapter,
		 * however, some integrated adapters may have 0xffff as
		 * their id and still be valid. Examples are on-board
		 * VGA of the 55sx, the integrated SCSI of the 56 & 57,
		 * and possibly also the 95 ULTIMEDIA.
		 */

		mca_dev->status = MCA_ADAPTER_NORMAL;
	}

	if((mca_dev->pos_id == 0xffff ||
	    mca_dev->pos_id == 0x0000) && mca_dev->slot >= MCA_MAX_SLOT_NR) {
		int j;

		for(j = 2; j < 8; j++) {
			if(mca_dev->pos[j] != 0xff) {
				mca_dev->status = MCA_ADAPTER_NORMAL;
				break;
			}
		}
	}

#if 0
	if(!(mca_dev->pos[2] & MCA_ENABLED)) {

		/* enabled bit is in POS 2 */

		mca_dev->status = MCA_ADAPTER_DISABLED;
	}
#endif
} /* mca_configure_adapter_status */

static void mca_power_write_pos(struct mca_device *mca_dev, int reg,
                             unsigned char byte)
{
	if(reg < 0 || reg >= 8)
		return;

	outb_p(byte, 0xB0400000+MCA_POS_REG(reg)+((mca_dev->slot&0xf)<<16));
	outb_p(0, 0xB04000E8);

	/* Update the global register list, while we have the byte */
	mca_dev->pos[reg] = byte;
}

/*
 *      mca_read_and_store_pos - read the POS registers into a memory buffer
 *      @pos: a char pointer to 8 bytes, contains the POS register value on
 *            successful return
 *
 *      Returns 1 if a card actually exists (i.e. the pos isn't
 *      all 0xff) or 0 otherwise
 */
static int mca_read_and_store_pos(int slot, unsigned char *pos) {
        int j;
        int found = 0;

        for(j=0; j<8; j++)
                pos[j] = inb_p(0xB0400000+MCA_POS_REG(j)+(slot<<16));

	printk("MCA slot %d: %02x %02x %02x %02x %02x %02x %02x %02x\n",
		slot, 
		(unsigned int)(pos[0]),
		(unsigned int)(pos[1]),
		(unsigned int)(pos[2]),
		(unsigned int)(pos[3]),
		(unsigned int)(pos[4]),
		(unsigned int)(pos[5]),
		(unsigned int)(pos[6]),
		(unsigned int)(pos[7])
		);

        for(j=0; j<8; j++) {
                if(pos[j] != 0xff) {
                        /* 0xff all across means no device. 0x00 means
			 * something's broken, but a device is
			 * probably there.  However, if you get 0x00
			 * from a motherboard register it won't matter
			 * what we find.  For the record, on the
			 * 57SLC, the integrated SCSI adapter has
			 * 0xffff for the adapter ID, but nonzero for
			 * other registers.  */

			found = 1;
		}
	}
	return found;
}

static unsigned char mca_power_read_pos(struct mca_device *mca_dev, int reg)
{
	unsigned char byte;

	if(reg < 0 || reg >= 8)
		return 0;

	byte = inb_p(0xB0400000+MCA_POS_REG(reg)+(mca_dev->slot<<16));

	mca_dev->pos[reg] = byte;

	return byte;
}

/* for the primary MCA bus, we have identity transforms */
static int mca_dummy_transform_irq(struct mca_device * mca_dev, int irq)
{
	return irq;
}

static int mca_dummy_transform_ioport(struct mca_device * mca_dev, int port)
{
	return port;
}

static void *mca_dummy_transform_memory(struct mca_device * mca_dev, void *mem)
{
	return mem;
}


static int __init mca_init(void)
{
	int i,j;
	struct mca_bus *bus;
	struct mca_device *mca_dev;
	unsigned char pos[8];
	unsigned long *piv;

	if (mca_system_init()) {
		printk(KERN_ERR "MCA bus system initialisation failed\n");
		return -ENODEV;
	}

	if (!MCA_bus)
	{
		printk(KERN_INFO "No mca bus present\n");
		return -ENODEV;
	}

	printk(KERN_INFO "Micro Channel bus detected.\n");

	/*
	 * Identity map the interrupts but the lower 4.
	 */
	piv = (unsigned long *)0xB0400090;
	*piv++ = 0x10111213;
	*piv++ = 0x04050607;
	*piv++ = 0x08090A0B;
	*piv++ = 0x0C0D0E0F;

	/*
	 * Enable all interrupts.
	 */
	*(unsigned long *)(0xB0400080) = 0xFFFFFFFFUL;

	/* All MCA systems have at least a primary bus */
	bus = mca_attach_bus(MCA_PRIMARY_BUS);
	if (!bus)
		goto out_nomem;
	bus->default_dma_mask = 0xffffffffLL;
	bus->f.mca_write_pos = mca_power_write_pos;
	bus->f.mca_read_pos = mca_power_read_pos;
	bus->f.mca_transform_irq = mca_dummy_transform_irq;
	bus->f.mca_transform_ioport = mca_dummy_transform_ioport;
	bus->f.mca_transform_memory = mca_dummy_transform_memory;

	mca_show_iocc_config();

	/* Now loop over MCA slots: put each adapter into setup mode, and
	 * read its POS registers. Then put adapter setup off.
	 */
        for(i=0; i<MCA_MAX_SLOT_NR; i++) {
      		if(!mca_read_and_store_pos(i,pos))
			continue;

                mca_dev = kzalloc(sizeof(struct mca_device), GFP_ATOMIC);
                if(unlikely(!mca_dev))
                        goto out_nomem;

                for(j=0; j<8; j++)
                        mca_dev->pos[j]=pos[j];

                mca_dev->driver_loaded = 0;
                mca_dev->slot = i;
                mca_dev->pos_register = 0;
                mca_configure_adapter_status(mca_dev);
                mca_register_device(MCA_PRIMARY_BUS, mca_dev);
        }

#if 0
        for (i = 0; i < MCA_STANDARD_RESOURCES; i++)
                request_resource(&ioport_resource, mca_standard_resources + i);
#endif
        mca_do_proc_init();


	return 0;

out_nomem:
	printk(KERN_EMERG "Failed memory allocation in MCA setup!\n");
	return -ENOMEM;
}


subsys_initcall(mca_init);

