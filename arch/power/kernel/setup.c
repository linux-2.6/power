/*
 * Common prep boot and setup code.
 */

#include <linux/module.h>
#include <linux/string.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/reboot.h>
#include <linux/delay.h>
#include <linux/initrd.h>
//include <linux/ide.h>
#include <linux/screen_info.h>
#include <linux/bootmem.h>
#include <linux/seq_file.h>
#include <linux/root_dev.h>
#include <linux/cpu.h>
#include <linux/console.h>

#include <linux/irq.h>	// only for irq chip stuff

//include <asm/residual.h>
#include <asm/io.h>
#include <asm/prom.h>
#include <asm/processor.h>
#include <asm/pgtable.h>
#include <asm/bootinfo.h>
#include <asm/setup.h>
//include <asm/smp.h>
#include <asm/elf.h>
#include <asm/cputable.h>
//include <asm/bootx.h>
//include <asm/btext.h>
#include <asm/machdep.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/sections.h>
//include <asm/nvram.h>
#include <asm/xmon.h>
//include <asm/ocp.h>

#define USES_PPC_SYS (defined(CONFIG_MPC10X_BRIDGE) || defined(CONFIG_8260) || \
		      defined(CONFIG_PPC_MPC52xx))

#if USES_PPC_SYS
#include <asm/ppc_sys.h>
#endif

#if defined CONFIG_KGDB
#include <asm/kgdb.h>
#endif

extern void platform_init(unsigned long r3, unsigned long r4,
		unsigned long r5, unsigned long r6, unsigned long r7);
extern void reloc_got2(unsigned long offset);

/* Used with the BI_MEMSIZE bootinfo parameter to store the memory
   size value reported by the boot loader. */
unsigned long boot_mem_size;

unsigned long ISA_DMA_THRESHOLD;
unsigned int DMA_MODE_READ;
unsigned int DMA_MODE_WRITE;

#ifdef CONFIG_PPC_PREP
extern void prep_init(unsigned long r3, unsigned long r4,
		unsigned long r5, unsigned long r6, unsigned long r7);

dev_t boot_dev;
#endif /* CONFIG_PPC_PREP */

int have_of;
EXPORT_SYMBOL(have_of);

#ifdef __DO_IRQ_CANON
int ppc_do_canonicalize_irqs;
EXPORT_SYMBOL(ppc_do_canonicalize_irqs);
#endif

#ifdef CONFIG_VGA_CONSOLE
unsigned long vgacon_remap_base;
#endif

struct machdep_calls ppc_md;

/*
 * These are used in binfmt_elf.c to put aux entries on the stack
 * for each elf executable being started.
 */
int dcache_bsize;
int icache_bsize;
int ucache_bsize;

#if defined(CONFIG_VGA_CONSOLE) || defined(CONFIG_FB_VGA16) || \
    defined(CONFIG_FB_VGA16_MODULE) || defined(CONFIG_FB_VESA)
struct screen_info screen_info = {
	0, 25,			/* orig-x, orig-y */
	0,			/* unused */
	0,			/* orig-video-page */
	0,			/* orig-video-mode */
	80,			/* orig-video-cols */
	0,0,0,			/* ega_ax, ega_bx, ega_cx */
	25,			/* orig-video-lines */
	1,			/* orig-video-isVGA */
	16			/* orig-video-points */
};
#endif /* CONFIG_VGA_CONSOLE || CONFIG_FB_VGA16 || CONFIG_FB_VESA */

void machine_restart(char *cmd)
{
#ifdef CONFIG_NVRAM
	nvram_sync();
#endif
	ppc_md.restart(cmd);
}

static void ppc_generic_power_off(void)
{
	ppc_md.power_off();
}

void machine_halt(void)
{
#ifdef CONFIG_NVRAM
	nvram_sync();
#endif
	ppc_md.halt();
}

void (*pm_power_off)(void) = ppc_generic_power_off;

void machine_power_off(void)
{
#ifdef CONFIG_NVRAM
	nvram_sync();
#endif
	if (pm_power_off)
		pm_power_off();
	ppc_generic_power_off();
}

#ifdef CONFIG_TAU
extern u32 cpu_temp(unsigned long cpu);
extern u32 cpu_temp_both(unsigned long cpu);
#endif /* CONFIG_TAU */

int show_cpuinfo(struct seq_file *m, void *v)
{
	int i = (int) v - 1;
	int err = 0;
	unsigned int pvr;
	unsigned short maj, min;
	unsigned long lpj;

	if (i >= NR_CPUS) {
		/* Show summary information */
#ifdef CONFIG_SMP
		unsigned long bogosum = 0;
		for_each_online_cpu(i)
			bogosum += cpu_data[i].loops_per_jiffy;
		seq_printf(m, "total bogomips\t: %lu.%02lu\n",
			   bogosum/(500000/HZ), bogosum/(5000/HZ) % 100);
#endif /* CONFIG_SMP */

		if (ppc_md.show_cpuinfo != NULL)
			err = ppc_md.show_cpuinfo(m);
		return err;
	}

#ifdef CONFIG_SMP
	if (!cpu_online(i))
		return 0;
	pvr = cpu_data[i].pvr;
	lpj = cpu_data[i].loops_per_jiffy;
#else
	pvr = mfspr(SPRN_PVR);
	lpj = loops_per_jiffy;
#endif

	seq_printf(m, "processor\t: %d\n", i);
	seq_printf(m, "cpu\t\t: ");

#if 0
	if (cur_cpu_spec->pvr_mask)
		seq_printf(m, "%s", cur_cpu_spec->cpu_name);
	else
		seq_printf(m, "unknown (%08x)", pvr);
#ifdef CONFIG_ALTIVEC
	if (cur_cpu_spec->cpu_features & CPU_FTR_ALTIVEC)
		seq_printf(m, ", altivec supported");
#endif
	seq_printf(m, "\n");
#endif

#ifdef CONFIG_TAU
	if (cur_cpu_spec->cpu_features & CPU_FTR_TAU) {
#ifdef CONFIG_TAU_AVERAGE
		/* more straightforward, but potentially misleading */
		seq_printf(m,  "temperature \t: %u C (uncalibrated)\n",
			   cpu_temp(i));
#else
		/* show the actual temp sensor range */
		u32 temp;
		temp = cpu_temp_both(i);
		seq_printf(m, "temperature \t: %u-%u C (uncalibrated)\n",
			   temp & 0xff, temp >> 16);
#endif
	}
#endif /* CONFIG_TAU */

	if (ppc_md.show_percpuinfo != NULL) {
		err = ppc_md.show_percpuinfo(m, i);
		if (err)
			return err;
	}

	/* If we are a Freescale core do a simple check so
	 * we dont have to keep adding cases in the future */
	if ((PVR_VER(pvr) & 0x8000) == 0x8000) {
		maj = PVR_MAJ(pvr);
		min = PVR_MIN(pvr);
	} else {
		switch (PVR_VER(pvr)) {
			case 0x0020:	/* 403 family */
				maj = PVR_MAJ(pvr) + 1;
				min = PVR_MIN(pvr);
				break;
			case 0x1008:	/* 740P/750P ?? */
				maj = ((pvr >> 8) & 0xFF) - 1;
				min = pvr & 0xFF;
				break;
			default:
				maj = (pvr >> 8) & 0xFF;
				min = pvr & 0xFF;
				break;
		}
	}

	seq_printf(m, "revision\t: %hd.%hd (pvr %04x %04x)\n",
		   maj, min, PVR_VER(pvr), PVR_REV(pvr));

	seq_printf(m, "bogomips\t: %lu.%02lu\n",
		   lpj / (500000/HZ), (lpj / (5000/HZ)) % 100);

#if USES_PPC_SYS
	if (cur_ppc_sys_spec->ppc_sys_name)
		seq_printf(m, "chipset\t\t: %s\n",
			cur_ppc_sys_spec->ppc_sys_name);
#endif

#ifdef CONFIG_SMP
	seq_printf(m, "\n");
#endif

	return 0;
}

static void *c_start(struct seq_file *m, loff_t *pos)
{
	int i = *pos;

	return i <= NR_CPUS? (void *) (i + 1): NULL;
}

static void *c_next(struct seq_file *m, void *v, loff_t *pos)
{
	++*pos;
	return c_start(m, pos);
}

static void c_stop(struct seq_file *m, void *v)
{
}

const struct seq_operations cpuinfo_op = {
	.start =c_start,
	.next =	c_next,
	.stop =	c_stop,
	.show =	show_cpuinfo,
};

/*
 * We're called here very early in the boot.  We determine the machine
 * type and call the appropriate low-level setup functions.
 *  -- Cort <cort@fsmlabs.com>
 *
 * Note that the kernel may be running at an address which is different
 * from the address that it was linked at, so we must use RELOC/PTRRELOC
 * to access static data (including strings).  -- paulus
 */
__init
unsigned long
early_init(int r3, int r4, int r5)
{
	unsigned long phys;
	unsigned long offset = reloc_offset();
	struct cpu_spec *spec;

	/* Default */
	phys = offset + KERNELBASE;

	/* First zero the BSS -- use memset, some arches don't have
	 * caches on yet */
	memset_io(PTRRELOC(&__bss_start), 0, _end - __bss_start);

	{
		unsigned long *p = (unsigned long *)PTRRELOC(&_end);
		for ( ; p < (unsigned long *)0x02e00000; p++ )
		{
			*p = (unsigned long)p;
		}
	}

	/*
	 * Identify the CPU type and fix up code sections
	 * that depend on which cpu we have.
	 */
#if defined(CONFIG_440EP) && defined(CONFIG_PPC_FPU)
        /* We pass the virtual PVR here for 440EP as 440EP and 440GR have
	 * identical PVRs and there is no reliable way to check for the FPU
	 */
	spec = identify_cpu(offset, (mfspr(SPRN_PVR) | 0x8));
#elif defined(CONFIG_POWER)
	/*
	 * On power we have from PVR and have to rely on the IPL to pass
	 * the required information
	 */
	spec = identify_cpu(offset, r3);
#else
        spec = identify_cpu(offset, mfspr(SPRN_PVR));
#endif
        do_feature_fixups(spec->cpu_features,
                          PTRRELOC(&__start___ftr_fixup),
                          PTRRELOC(&__stop___ftr_fixup));

        return phys;
}

void __init
platform_init(unsigned long r3, unsigned long r4, unsigned long r5,
	      unsigned long r6, unsigned long r7)
{
#ifdef CONFIG_BOOTX_TEXT
	if (boot_text_mapped) {
		btext_clearscreen();
		btext_welcome();
	}
#endif

	/* TODO: This does reduce the available amount of memory.
	 *       It would be better to allocate the pages by
	 *       the mempiece mechanism. But this seems to be
	 *       uninitialized here and to have no hoook for
	 *       the platform.
	 */

	/* Allocate the temporary hash map */
	r3 -= 32 * 1024;
	r3 &= ~(32 * 1024 - 1);
	/* Allocate the temporary page table */
	r3 -= 1024 * 1024;
	r3 &= ~(1024 * 1024 - 1);

	boot_mem_size = r3;
}

extern void rs6000_serial_putc(int c);
extern void setled(int c);

static void __init
rs6000_progress(char *txt, unsigned short dig)
{
	char c;

	setled(((unsigned long)dig)<<(16+4));

	while ( (c = *txt++) ) rs6000_serial_putc(c);
	rs6000_serial_putc(13);
	rs6000_serial_putc(10);
}

/*
 *  * Early boot console based on udbg
 *   */
static void rs6000_console_write(struct console *con, const char *s,
                unsigned int n)
{
	unsigned int i;

	for ( i=0; i<n; i++ )
	{
		if ( s[i] == '\n' ) rs6000_serial_putc('\r');
		rs6000_serial_putc(s[i]);
	}
}

static struct console rs6000_console = {
        .name   = "s0",
        .write  = rs6000_console_write,
        .flags  = CON_PRINTBUFFER | CON_ENABLED /* | CON_BOOT */,
        .index  = -1,
};

static void rs6000_mask_irq(unsigned int irq_nr) {};
static void rs6000_unmask_irq(unsigned int irq_nr) {};
static void rs6000_mask_and_ack_irq(unsigned int irq_nr) {};

static void rs6000_mca_rfi(unsigned int irq_nr)
{
	/*
	 * TODO: It's not sure this is right here since it
	 *       is called once for each interrupt.
	 *       The original system does it only after
	 *       registering all pending MCA interrupts.
	 */
	unsigned long tmp2;
	asm volatile (
	    "    lwz  %0, 0(%1) "
	    :   "=r" (tmp2)
	    :   "b" (0xB040008C) );
}


static struct irq_chip mca_pic = {
	.typename	= " mca      ",
	.mask		= rs6000_mask_irq,
	.disable	= rs6000_mask_irq,
	.unmask		= rs6000_unmask_irq,
//	.mask_ack	= rs6000_mask_and_ack_irq,
	.end		= rs6000_mca_rfi,
};


static void __init
rs6000_setup_arch(void)
{
	/*
	 * Make the MCA PIC code handle all interrups that may
	 * come from the MCA bus
	 */
	int i;
	for ( i=4; i<4+16; i++ )
	{
//		set_irq_chip_and_handler(i, &mca_pic, handle_level_irq);
		set_irq_chip(i, &mca_pic);
		irq_desc[i].status |= IRQ_LEVEL;
	}
}

static void __init
rs6000_early_init(void)
{
        register_console(&rs6000_console);
}

static int __init register_rs6000_console(void)
{
	return add_preferred_console("s0", 0, NULL);
}

static void __init rs6000_init_irq(void)
{
#if 0
	unsigned long e0 = 0xffffffffUL, e1 = 0xffffffffUL;
	unsigned long busseg = 0x80000000UL;
	unsigned long busofs = 0xF0000000UL;
	unsigned long tmp;

	asm volatile (
	      "	mfsr	%0, 15		\n"
	      "	mtsr	15, %3		\n"
	      "	ics			\n"
	      "	stw	%1,0(%4)	\n"
	      "	stw	%2,4(%4)	\n"
	      "	mtsr	15, %0		\n"
	      "	ics "
	        : "=&r" (tmp)
		: "r" (e0), "r" (e1), "r" (busseg), "b" (busofs)
		 );
#endif

}

static int rs6000_get_irq(void)
{
	unsigned long eis[2];
	unsigned long busseg = 0x80000000UL;
//	unsigned long busofs = 0xF0000000UL;
	unsigned long busofs = 0xF0000000UL;
	unsigned long tmp, tmp2;

	asm volatile (
#if 0
	    "	mfsr	%0, 15		\n"	/* Save sr15 */
	    "	mtsr	15, %3		\n"	/* Set new sr15 */
	    "	ics			\n"
#endif
	    "	lwz	%1,0x10(%4)	\n"	/* Get EIS0 */
	    "	lwz	%2,0x14(%4)	\n"	/* Get EIS1 */
#if 0
	    "	mtsr	15, %0		\n"	/* Restore old sr15 */
#endif
	    "	ics 			\n"
	    "	cntlzw	%0,%1		\n"
	    "	cmpwi	%0,32		\n"
	    "	bne	1f		\n"
	    "	cntlzw  %0,%2		\n"
	    "	ai	%0,%0,32	\n"
	    "1: 			\n"
	    :	"=&r" (tmp), "=&r" (eis[0]), "=&r" (eis[1])
	    :   "r" (busseg), "b" (busofs)
	    :   "cc","memory" );

	if ( tmp == 64 ) 
	{
		printk("IRQ triggered but noone claims to have fired it!\n");
		return NO_IRQ;
	}

//printk("IRQ: %08lx%08lx\n", eis[0], eis[1]);

/*
	eis[0] = eis[1] = 0xffffffffUL;
	if ( tmp < 32 )
		eis[0] &= ~(0x80000000UL >> tmp);
	else if ( tmp < 64 )
		eis[1] &= ~(0x80000000UL >> (tmp-32));
*/

	if ( tmp < 32 )
		eis[0] &= ~(0x80000000UL >> tmp);
	else if ( tmp < 64 )
		eis[1] &= ~(0x80000000UL >> (tmp-32));
/*
	eis[0] = eis[1] = 0;
	if ( tmp < 32 )
		eis[0] = (0x80000000UL >> tmp);
	else if ( tmp < 64 )
		eis[1] = (0x80000000UL >> (tmp-32));
*/
	asm volatile (
	    "	mfsr	%0, 15		\n"	/* Save sr15 */
	    "	mtsr	15, %3		\n"	/* Set new sr15 */
	    "	ics			\n"
	    "	stw	%1,0x10(%4)	\n"	/* Store EIS0 */
	    "	stw	%2,0x14(%4)	\n"	/* Store EIS1 */
	    "	dcs 			\n"
	    "	mtsr	15, %0		\n"	/* Restore old sr15 */
	    "	ics 			\n"
	    :	"=&r" (tmp2)
	    :   "r" (eis[0]), "r" (eis[1]), "r" (busseg), "b" (busofs)
	    :   "cc","memory" );


	return (int)tmp;
}

console_initcall(register_rs6000_console);


/*
 * Find out what kind of machine we're on and save any data we need
 * from the early boot process (devtree is copied on pmac by prom_init()).
 * This is called very early on the boot process, after a minimal
 * MMU environment has been set up but before MMU_init is called.
 */
void __init
machine_init(unsigned long r3, unsigned long r4, unsigned long r5,
	     unsigned long r6, unsigned long r7)
{
#ifdef CONFIG_CMDLINE
	strlcpy(cmd_line, CONFIG_CMDLINE, sizeof(cmd_line));
#endif /* CONFIG_CMDLINE */

#ifdef CONFIG_6xx
	ppc_md.power_save = ppc6xx_idle;
#endif
#ifdef CONFIG_POWER
	ppc_md.progress = &rs6000_progress;
	ppc_md.setup_arch = &rs6000_setup_arch;
	ppc_md.init_early = &rs6000_early_init;
	ppc_md.init_IRQ = &rs6000_init_irq;
	ppc_md.get_irq = &rs6000_get_irq;
#endif

	platform_init(r3, r4, r5, r6, r7);

	if (ppc_md.progress)
		ppc_md.progress("id mach(): done", 0x200);
}
#ifdef CONFIG_BOOKE_WDT
/* Checks wdt=x and wdt_period=xx command-line option */
int __init early_parse_wdt(char *p)
{
	if (p && strncmp(p, "0", 1) != 0)
	       booke_wdt_enabled = 1;

	return 0;
}
early_param("wdt", early_parse_wdt);

int __init early_parse_wdt_period (char *p)
{
	if (p)
		booke_wdt_period = simple_strtoul(p, NULL, 0);

	return 0;
}
early_param("wdt_period", early_parse_wdt_period);
#endif	/* CONFIG_BOOKE_WDT */

/* Checks "l2cr=xxxx" command-line option */
int __init ppc_setup_l2cr(char *str)
{
	if (cpu_has_feature(CPU_FTR_L2CR)) {
		unsigned long val = simple_strtoul(str, NULL, 0);
		printk(KERN_INFO "l2cr set to %lx\n", val);
		_set_L2CR(0);		/* force invalidate by disable cache */
		_set_L2CR(val);		/* and enable it */
	}
	return 1;
}
__setup("l2cr=", ppc_setup_l2cr);

#ifdef CONFIG_GENERIC_NVRAM

/* Generic nvram hooks used by drivers/char/gen_nvram.c */
unsigned char nvram_read_byte(int addr)
{
	if (ppc_md.nvram_read_val)
		return ppc_md.nvram_read_val(addr);
	return 0xff;
}
EXPORT_SYMBOL(nvram_read_byte);

void nvram_write_byte(unsigned char val, int addr)
{
	if (ppc_md.nvram_write_val)
		ppc_md.nvram_write_val(addr, val);
}
EXPORT_SYMBOL(nvram_write_byte);

void nvram_sync(void)
{
	if (ppc_md.nvram_sync)
		ppc_md.nvram_sync();
}
EXPORT_SYMBOL(nvram_sync);

#endif /* CONFIG_NVRAM */

static struct cpu cpu_devices[NR_CPUS];

int __init ppc_init(void)
{
	int i;

	/* clear the progress line */
	if ( ppc_md.progress ) ppc_md.progress("             ", 0xffff);

	/* register CPU devices */
	for_each_possible_cpu(i)
		register_cpu(&cpu_devices[i], i);

	/* call platform init */
	if (ppc_md.init != NULL) {
		ppc_md.init();
	}
	return 0;
}

arch_initcall(ppc_init);

/* Warning, IO base is not yet inited */
void __init setup_arch(char **cmdline_p)
{
	extern char *klimit;
	extern void do_init_bootmem(void);

	/* so udelay does something sensible, assume <= 1000 bogomips */
	loops_per_jiffy = 500000000 / HZ;

	if (ppc_md.init_early)
		ppc_md.init_early();

#ifdef CONFIG_XMON
	xmon_init(1);
	if (strstr(cmd_line, "xmon"))
		xmon(NULL);
#endif /* CONFIG_XMON */
	if ( ppc_md.progress ) ppc_md.progress("setup_arch: enter", 0x3eab);

#if defined(CONFIG_KGDB)
	if (ppc_md.kgdb_map_scc)
		ppc_md.kgdb_map_scc();
	set_debug_traps();
	if (strstr(cmd_line, "gdb")) {
		if (ppc_md.progress)
			ppc_md.progress("setup_arch: kgdb breakpoint", 0x4000);
		printk("kgdb breakpoint activated\n");
		breakpoint();
	}
#endif

	/*
	 * Set cache line size based on type of cpu as a default.
	 * Systems with OF can look in the properties on the cpu node(s)
	 * for a possibly more accurate value.
	 */
#if 1
	if (! cpu_has_feature(CPU_FTR_UNIFIED_ID_CACHE)) {
		dcache_bsize = cur_cpu_spec->dcache_bsize;
		icache_bsize = cur_cpu_spec->icache_bsize;
		ucache_bsize = 0;
	} else
		ucache_bsize = dcache_bsize = icache_bsize
			= cur_cpu_spec->dcache_bsize;
#else
		ucache_bsize = dcache_bsize = icache_bsize = 32;
#endif

	/* reboot on panic */
	panic_timeout = 180;

	init_mm.start_code = PAGE_OFFSET;
	init_mm.end_code = (unsigned long) _etext;
	init_mm.end_data = (unsigned long) _edata;
	init_mm.brk = (unsigned long) klimit;

	/* Save unparsed command line copy for /proc/cmdline */
	strlcpy(boot_command_line, cmd_line, COMMAND_LINE_SIZE);
	*cmdline_p = cmd_line;

	parse_early_param();

	/* set up the bootmem stuff with available memory */
	do_init_bootmem();
	if ( ppc_md.progress ) ppc_md.progress("setup_arch: bootmem", 0x3eab);

#ifdef CONFIG_PPC_OCP
	/* Initialize OCP device list */
	ocp_early_init();
	if ( ppc_md.progress ) ppc_md.progress("ocp: exit", 0x3eab);
#endif

#ifdef CONFIG_DUMMY_CONSOLE
	conswitchp = &dummy_con;
#endif

	ppc_md.setup_arch();
	if ( ppc_md.progress ) ppc_md.progress("arch: exit", 0x3eab);

	paging_init();
}
