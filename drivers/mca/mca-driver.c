/* -*- mode: c; c-basic-offset: 8 -*- */

/*
 * MCA driver support functions for sysfs.
 *
 * (C) 2002 James Bottomley <James.Bottomley@HansenPartnership.com>
 *
**-----------------------------------------------------------------------------
**  
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
**-----------------------------------------------------------------------------
 */

#include <linux/device.h>
#include <linux/mca.h>
#include <linux/module.h>

#ifdef CONFIG_PNPMCA
#include <linux/mod_devicetable.h>
#include <linux/pnp.h>

/*
 * Probe for the device
 *
 * This calls the original routine given in the mca_device structure. This
 * way drivers do not need to change. They can read their resources from
 * the POS registers by themselve. They need to do so for the unknown
 * resource types anyway.
 *
 */
static int mca_pnp_probe(struct pnp_dev *pdev, const struct pnp_device_id *id)
{
	struct mca_device *mca_dev = pdev->data;
	struct mca_driver *mca_drv = (struct mca_driver *)id->driver_data;
	struct pnp_device_id id2;

	/*
	 * We need to set the index member of the mca_dev structure
	 * correctly, so compare IDs till match.
	 */
	for ( mca_dev->index = 0; likely(mca_drv->id_table[mca_dev->index]); mca_dev->index++ )
	{
		snprintf(id2.id, PNP_ID_LEN, "MCA%04hX", mca_drv->id_table[mca_dev->index]);
		if ( !strncmp(id2.id, id->id, PNP_ID_LEN) )
		{
			return mca_drv->driver.probe(&mca_dev->dev);
		}
	}

	printk(KERN_ERR "mca_pnp_probe: ID mismatch\n");
	return -ENODEV;
}

int mca_register_driver(struct mca_driver *mca_drv)
{
	int r;
	struct pnp_driver *driver_pnp;
	int nr_device_ids;
	struct pnp_device_id *device_ids_pnp;
	int i;

	if (MCA_bus) {
		mca_drv->driver.bus = &mca_bus_type;
		driver_pnp = kzalloc(sizeof (struct pnp_driver), GFP_KERNEL);
		for ( nr_device_ids = 0; mca_drv->id_table[nr_device_ids] != 0; nr_device_ids++ )
			;
		device_ids_pnp = kzalloc(nr_device_ids * sizeof (struct pnp_device_id), GFP_KERNEL);
		for ( i = 0; i < nr_device_ids; i++ )
		{
			snprintf(device_ids_pnp[i].id, PNP_ID_LEN, "MCA%04hX", mca_drv->id_table[i]);
			device_ids_pnp[i].driver_data = (kernel_ulong_t)mca_drv;
		}

		driver_pnp->probe = mca_pnp_probe;
		driver_pnp->name  = (char*)mca_drv->driver.name;
		driver_pnp->id_table = device_ids_pnp;

		if ((r= pnp_register_driver(driver_pnp)) < 0)
		{
			printk("pnp_register_driver failed: %d\n", r);
			return r;
		}
		mca_drv->integrated_id = 0;
	}

	return 0;
}

#else		// No PNP support for MCA

int mca_register_driver(struct mca_driver *mca_drv)
{
	int r;

	if (MCA_bus) {
		mca_drv->driver.bus = &mca_bus_type;
		if ((r = driver_register(&mca_drv->driver)) < 0)
			return r;
		mca_drv->integrated_id = 0;
	}

	return 0;
}

#endif
EXPORT_SYMBOL(mca_register_driver);

int mca_register_driver_integrated(struct mca_driver *mca_driver,
				   int integrated_id)
{
	int r = mca_register_driver(mca_driver);

	if (!r)
		mca_driver->integrated_id = integrated_id;

	return r;
}
EXPORT_SYMBOL(mca_register_driver_integrated);

void mca_unregister_driver(struct mca_driver *mca_drv)
{
	if (MCA_bus)
		driver_unregister(&mca_drv->driver);
}
EXPORT_SYMBOL(mca_unregister_driver);
