#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/mutex.h>
#include <asm/io.h>

#include <linux/pnp.h>
#include <linux/mca.h>

MODULE_AUTHOR("Michael Mueller <malware@t-online.de>");
MODULE_DESCRIPTION("Generic Microchannel Plug & Play support");
MODULE_LICENSE("GPL");

struct mca_resource {
	unsigned long value;
	unsigned long length;
	unsigned short pos[4];
	unsigned short pos_mask[4];
};

struct mca_resource_map_entry {
	unsigned short mca_id;	// POS_ID
	char *name;
	struct mca_resource unk_resources;
	struct mca_resource *io_resources;
	struct mca_resource *irq_resources;
	struct mca_resource *mem_resources;
	struct mca_resource *dma_resources;
};

struct mca_resource io_resources_8EF5[] = {
	{
	.value    = 0x7280,
	.length	  = 0xF,
	.pos      = { [0] = 0x00 },
	.pos_mask = { [0] = 0x0E }
	},
	{
	.value    = 0x7290,
	.length	  = 0xF,
	.pos      = { [0] = 0x02 },
	.pos_mask = { [0] = 0x0E }
	},
	{
	.value    = 0x7680,
	.length	  = 0xF,
	.pos      = { [0] = 0x04 },
	.pos_mask = { [0] = 0x0E }
	},
	{
	.value    = 0x7690,
	.length	  = 0xF,
	.pos      = { [0] = 0x06 },
	.pos_mask = { [0] = 0x0E }
	},
	{
	.value    = 0x7A80,
	.length	  = 0xF,
	.pos      = { [0] = 0x08 },
	.pos_mask = { [0] = 0x0E }
	},
	{
	.value    = 0x7A90,
	.length	  = 0xF,
	.pos      = { [0] = 0x0A },
	.pos_mask = { [0] = 0x0E }
	},
	{
	.value    = 0x7E80,
	.length	  = 0xF,
	.pos      = { [0] = 0x0C },
	.pos_mask = { [0] = 0x0E }
	},
	{
	.value    = 0x7E90,
	.length	  = 0xF,
	.pos      = { [0] = 0x0E },
	.pos_mask = { [0] = 0x0E }
	},
	{
	.value	  = 0,
	.length	  = 0
	}
};

struct mca_resource irq_resources_8EF5[] = {
	{
	.value    = 10,
	.pos	  = { [3] = 0x04 },
	.pos_mask = { [3] = 0x0C }
	},
	{
	.value    = 11,
	.pos	  = { [3] = 0x08 },
	.pos_mask = { [3] = 0x0C }
	},
	{
	.value    = 12,
	.pos	  = { [3] = 0x0C },
	.pos_mask = { [3] = 0x0C }
	},
	{
	.value    = 9,
	.pos	  = { [3] = 0x00 },
	.pos_mask = { [3] = 0x0C }
	},
	{
	.value	  = 0,
	.length	  = 0
	}
};

struct mca_resource mem_resources_8EF5[] = {
	{
	.value    = 0xD0000,
	.length   = 0x04000,
	.pos      = { [0] = 0x40, [2] = 0x01, [3] = 0x00 },
	.pos_mask = { [0] = 0x70, [2] = 0x01, [3] = 0x30 }
	},
	{
	.value    = 0xD4000,
	.length   = 0x04000,
	.pos      = { [0] = 0x50, [2] = 0x01, [3] = 0x00 },
	.pos_mask = { [0] = 0x70, [2] = 0x01, [3] = 0x30 }
	},
	{
	.value    = 0xD8000,
	.length   = 0x04000,
	.pos      = { [0] = 0x60, [2] = 0x01, [3] = 0x00 },
	.pos_mask = { [0] = 0x70, [2] = 0x01, [3] = 0x30 }
	},
	{
	.value    = 0xDC000,
	.length   = 0x04000,
	.pos      = { [0] = 0x70, [2] = 0x01, [3] = 0x00 },
	.pos_mask = { [0] = 0x70, [2] = 0x01, [3] = 0x30 }
	},
	{
	.value    = 0xC8000,
	.length   = 0x04000,
	.pos      = { [0] = 0x20, [2] = 0x01, [3] = 0x00 },
	.pos_mask = { [0] = 0x70, [2] = 0x01, [3] = 0x30 }
	},
	{
	.value    = 0xCC000,
	.length   = 0x04000,
	.pos      = { [0] = 0x30, [2] = 0x01, [3] = 0x00 },
	.pos_mask = { [0] = 0x70, [2] = 0x01, [3] = 0x30 }
	},
	{
	.value    = 0xC0000,
	.length   = 0x04000,
	.pos      = { [0] = 0x00, [2] = 0x01, [3] = 0x00 },
	.pos_mask = { [0] = 0x70, [2] = 0x01, [3] = 0x30 }
	},
	{
	.value    = 0xC4000,
	.length   = 0x04000,
	.pos      = { [0] = 0x10, [2] = 0x01, [3] = 0x00 },
	.pos_mask = { [0] = 0x70, [2] = 0x01, [3] = 0x30 }
	},
	{
	.value	  = 0,
	.length	  = 0
	}
};

struct mca_resource dma_resources_8EF5[] = {
/* TODO: For testing purpose we make sure we get what we always had. */
/*
	{
	.value    = 8,
	.pos	  = { [1] = 0x08 },
	.pos_mask = { [1] = 0x0F }
	},
*/
	{
	.value    = 9,
	.pos	  = { [1] = 0x09 },
	.pos_mask = { [1] = 0x0F }
	},
/*
	{
	.value    = 10,
	.pos	  = { [1] = 0x0A },
	.pos_mask = { [1] = 0x0F }
	},
	{
	.value    = 11,
	.pos	  = { [1] = 0x0B },
	.pos_mask = { [1] = 0x0F }
	},
	{
	.value    = 12,
	.pos	  = { [1] = 0x0C },
	.pos_mask = { [1] = 0x0F }
	},
	{
	.value    = 13,
	.pos	  = { [1] = 0x0D },
	.pos_mask = { [1] = 0x0F }
	},
	{
	.value    = 14,
	.pos	  = { [1] = 0x0E },
	.pos_mask = { [1] = 0x0F }
	},
*/
	{
	.value	  = 0,
	.length	  = 0
	}
};

struct mca_resource_map_entry mca_resource_map[] = {
	{
	.mca_id = 0x8EF5,
	.name   = "IBM PS/2 Bus Master Adapter/A for Ethernet Networks",
	.unk_resources = {
		.pos      = { [0] = 0x00, [1] = 0x10, [2] = 0x12, [3] = 0x01 },
		.pos_mask = { [0] = 0x80, [1] = 0x10, [2] = 0x52, [3] = 0x03 }
		},
	.io_resources = io_resources_8EF5,
	.irq_resources = irq_resources_8EF5,
	.mem_resources = mem_resources_8EF5,
	.dma_resources = dma_resources_8EF5
	}
};

static inline int get_resource_map_index(int pos_id)
{
	int i;

	for ( i = 0;
	      i < sizeof mca_resource_map / sizeof mca_resource_map[0];
	      i++ )
		if ( mca_resource_map[i].mca_id == pos_id )
			return i;
	
	return -1;
}

static int pos_matches_resource(struct mca_resource *res, struct mca_device *dev)
{
	int i;
	for ( i=0; i<4; i++ )
	{
		if ( res->pos[i] !=
		     (mca_device_read_stored_pos(dev, 2+i) & res->pos_mask[i]) )
			return 0;
	}

	return 1;
}

static int __pnpmca_get_resources(struct mca_device *mca_dev,
                              struct pnp_resource_table *res)
{
	unsigned char POS;
	int re;
	struct mca_resource *pirq;

	POS = mca_device_read_stored_pos(mca_dev, 2);
	if ( !(POS&1) )
	{
#ifdef CONFIG_PNP_DEBUG
		printk(KERN_ERR "pnpmca_get_resource: Disable devices claim no resources\n");
#endif
		return -ENODEV;
	}

	re = get_resource_map_index(mca_dev->pos_id);
	if ( re < 0 )
	{
#ifdef CONFIG_PNP_DEBUG
		printk("pnpmca_get_resource: Devices structure not known\n");
#endif
		return -ENODEV;
	}

	pirq = mca_resource_map[re].irq_resources;
	while ( pirq && pirq->value )
	{
		if ( pos_matches_resource(pirq, mca_dev) )
			break;
		pirq++;
	}
#ifdef CONFIG_PNP_DEBUG
	if ( pirq && pirq->value )
		printk("Device is configured to IRQ %ld\n", pirq->value);
	else if ( pirq != mca_resource_map[re].irq_resources )
		printk("Device has unsupported IRQ setting configured\n");
	else
		printk("Device has no IRQ configured\n");
#endif
	/*
	 * TODO: This is not implemented yet.
	 *
	 * It is not even called yet. We should need it to avoid
	 * reconfiguring devices activated by the BIOS but to
	 * include their resources in our calculation.
	 */

	return -ENODEV;
}

static int pnpmca_get_resources(struct pnp_dev *dev,
                              struct pnp_resource_table *res)
{
	struct mca_device *mca_dev = dev->data;

	printk("pnpmca_get_resources called for device %d\n", dev->number);
	if ( !mca_dev )
	{
#ifdef CONFIG_PNP_DEBUG
		printk("pnpmca_get_resources: No mca_device attached, no way to do work.\n");
#endif
		return -ENODEV;
	}

	return __pnpmca_get_resources(mca_dev, res);
}

static int pnpmca_set_resources(struct pnp_dev *dev,
                              struct pnp_resource_table *res)
{
	struct mca_device *mca_dev = dev->data;
	struct mca_resource *r;
	unsigned char pos[4];	// POS-Registers
	int tmp, re, i;

#ifdef CONFIG_PNP_DEBUG
	printk("pnpmca_set_resources called for device %d\n", dev->number);
	printk("Selected options:\n");
	for ( tmp = 0; tmp < 8 /* ISAPNP_MAX_COUNT */
	      && (res->port_resource[tmp].
	         flags & (IORESOURCE_IO | IORESOURCE_UNSET)) == IORESOURCE_IO;
	      tmp++ )
	{
	      	printk("I/O: %04x\n", res->port_resource[tmp].start);
	}
	for ( tmp = 0; tmp < 8 /* ISAPNP_MAX_COUNT */
	      && (res->irq_resource[tmp].
	         flags & (IORESOURCE_IRQ | IORESOURCE_UNSET)) == IORESOURCE_IRQ;
	      tmp++ )
	{
	      	printk("IRQ: %d\n", res->irq_resource[tmp].start);
	}
	for ( tmp = 0; tmp < 8 /* ISAPNP_MAX_COUNT */
	      && (res->mem_resource[tmp].
	         flags & (IORESOURCE_MEM | IORESOURCE_UNSET)) == IORESOURCE_MEM;
	      tmp++ )
	{
	      	printk("MEM: %08x\n", res->mem_resource[tmp].start);
	}
	for ( tmp = 0; tmp < 8 /* ISAPNP_MAX_COUNT */
	      && (res->dma_resource[tmp].
	         flags & (IORESOURCE_DMA | IORESOURCE_UNSET)) == IORESOURCE_DMA;
	      tmp++ )
	{
	      	printk("DMA: %d\n", res->dma_resource[tmp].start);
	}
#endif

	re = get_resource_map_index(mca_dev->pos_id);
	if ( re < 0 )
	{
#ifdef CONFIG_PNP_DEBUG
		printk("pnpmca_set_resources: Devices structure not known\n");
#endif
		return -ENODEV;
	}

	if ( !mca_dev )
	{
#ifdef CONFIG_PNP_DEBUG
		printk("pnpmca_set_resources: No mca_device attached, no way to do work.\n");
#endif
		return -ENODEV;
	}

	memset(pos, 0, sizeof pos);
	pos[0] = 1;	// We want to active the device
	pos[3] = 0xc0;	// No error condition

	for ( tmp = 0; tmp < 8 /* ISAPNP_MAX_COUNT */
	      && (res->port_resource[tmp].
	         flags & (IORESOURCE_IO | IORESOURCE_UNSET)) == IORESOURCE_IO;
	      tmp++ )
	{
		r = mca_resource_map[re].io_resources;
		for ( ; r && r->value ; r++ )
		{
			if ( r->value  == res->port_resource[tmp].start )
			{
				for ( i=0; i<4; i++ )
				{
					pos[i] |= r->pos[i] & r->pos_mask[i];
				}
				break;
			}
		}
	}
	for ( tmp = 0; tmp < 8 /* ISAPNP_MAX_COUNT */
	      && (res->irq_resource[tmp].
	         flags & (IORESOURCE_IRQ | IORESOURCE_UNSET)) == IORESOURCE_IRQ;
	      tmp++ )
	{
		r = mca_resource_map[re].irq_resources;
		for ( ; r && r->value ; r++ )
		{
			if ( (r->value)  == res->irq_resource[tmp].start )
			{
				for ( i=0; i<4; i++ )
				{
					pos[i] |= r->pos[i] & r->pos_mask[i];
				}
				break;
			}
		}
	}
	for ( tmp = 0; tmp < 8 /* ISAPNP_MAX_COUNT */
	      && (res->mem_resource[tmp].
	         flags & (IORESOURCE_MEM | IORESOURCE_UNSET)) == IORESOURCE_MEM;
	      tmp++ )
	{
		r = mca_resource_map[re].mem_resources;
		for ( ; r && r->value ; r++ )
		{
			if ( r->value  == res->mem_resource[tmp].start )
			{
				for ( i=0; i<4; i++ )
				{
					pos[i] |= r->pos[i] & r->pos_mask[i];
				}
				break;
			}
		}
	}
	for ( tmp = 0; tmp < 8 /* ISAPNP_MAX_COUNT */
	      && (res->dma_resource[tmp].
	         flags & (IORESOURCE_DMA | IORESOURCE_UNSET)) == IORESOURCE_DMA;
	      tmp++ )
	{
		r = mca_resource_map[re].dma_resources;
		for ( ; r && r->value ; r++ )
		{
			if ( r->value == res->dma_resource[tmp].start )
			{
				for ( i=0; i<4; i++ )
				{
					pos[i] |= r->pos[i] & r->pos_mask[i];
				}
				break;
			}
		}
	}

	r = &mca_resource_map[re].unk_resources;
	for ( i=0; i<4; i++ )
	{
		pos[i] |= r->pos[i] & r->pos_mask[i];
	}

#ifdef CONFIG_PNP_DEBUG
	printk("Calculated POS values: %02x %02x %02x %02x\n",
		(unsigned int)pos[0], (unsigned int)pos[1],
		(unsigned int)pos[2], (unsigned int)pos[3]);
#endif

	for ( i=0; i<4; i++ )
	{
		mca_device_write_pos(mca_dev, i+2, pos[i]);
	}
	for ( ; i<6; i++ )
		mca_device_write_pos(mca_dev, i+2, 0);

	return 0;
}

static int pnpmca_disable_resources(struct pnp_dev *dev)
{
	struct mca_device *mca_dev = dev->data;

#ifdef CONFIG_PNP_DEBUG
	printk("pnpmca_disable_resources called for device %d\n", dev->number);
#endif

	if ( !mca_dev )
	{
#ifdef CONFIG_PNP_DEBUG
		printk("pnpmca_get_resources: No mca_device attached, no way to do work.\n");
#endif
	}

	mca_device_write_pos(mca_dev, 2, mca_device_read_pos(mca_dev,2) & ~0x01);
	return 0;
}

struct pnp_protocol pnpmca_protocol = {
	.name = "Microchannel Plug and Play",
	.get  = pnpmca_get_resources,
	.set  = pnpmca_set_resources,
	.disable = pnpmca_disable_resources,
};

static int mca_parse_resources(struct pnp_dev *pnp_dev, int pos_id)
{
	struct pnp_option *option;
	struct pnp_irq *irq;
	struct pnp_port *port;
	struct pnp_mem *mem;
	struct pnp_dma *dma;
	struct mca_resource *pio;
	struct mca_resource *pirq;
	struct mca_resource *pmem;
	struct mca_resource *pdma;
	struct mca_resource *ptmp;
	int re;
	
	re = get_resource_map_index(pos_id);
	if ( re < 0 ) return -ENODEV;

	strncpy(pnp_dev->name, mca_resource_map[re].name, PNP_NAME_LEN);

	for ( pio = mca_resource_map[re].io_resources; pio && pio->value ; pio++ )
	{
		DECLARE_BITMAP(irqmap, PNP_IRQ_NR);
		DECLARE_BITMAP(dmamap, PNP_DMA_NR);
		bitmap_zero(irqmap, PNP_IRQ_NR);
		bitmap_zero(dmamap, PNP_DMA_NR);

		pirq = mca_resource_map[re].irq_resources;
		for ( ptmp = pirq ; ptmp && ptmp->value ; ptmp++ )
			set_bit(ptmp->value, irqmap);	/* TODO: use mapping of MCA */

		pdma = mca_resource_map[re].dma_resources;
		for ( ptmp = pdma ; ptmp && ptmp->value ; ptmp++ )
			set_bit(ptmp->value, dmamap);

		pmem = mca_resource_map[re].mem_resources;
		do
		{
			option = pnp_register_dependent_option(pnp_dev, 0x100 | PNP_RES_PRIORITY_ACCEPTABLE);

			port = kzalloc(sizeof(struct pnp_port), GFP_KERNEL);
			port->min = port->max = pio->value;
			port->size = pio->length;
			port->align = 0;
			port->flags = 0;
			pnp_register_port_resource(option, port);

			if ( pirq && pirq->value )
			{
				irq = kzalloc(sizeof(struct pnp_irq), GFP_KERNEL);
				bitmap_copy(irq->map, irqmap, PNP_IRQ_NR);
				irq->flags = IORESOURCE_IRQ_HIGHLEVEL;
				pnp_register_irq_resource(option, irq);
			}

			if ( pmem && pmem->value )
			{
				mem = kzalloc(sizeof(struct pnp_mem), GFP_KERNEL);
				mem->min = mem->max = pmem->value;
				mem->size = pmem->length;
				pnp_register_mem_resource(option, mem);
				pmem++;
			}

			if ( pdma && pdma->value )
			{
				dma = kzalloc(sizeof(struct pnp_dma), GFP_KERNEL);
				bitmap_copy(dma->map, dmamap, PNP_DMA_NR);
				pnp_register_dma_resource(option, dma);
			}
		}
		while ( pmem && pmem->value );
	}

	return re;

}

static int pnpmca_add_device_callback(struct device *dev, void *data)
{
	struct mca_device *mca_dev = to_mca_device(dev);
	struct pnp_card *card;
	struct pnp_dev *pnp_dev;
	struct pnp_id *dev_id;
	struct pnp_id *dev_id2;
	int error = -ENOMEM;

	card    = kzalloc(sizeof(struct pnp_card), GFP_KERNEL);
	if ( !card )
	{
#ifdef CONFIG_PNP_DEBUG
		printk(KERN_ERR "Could not allocated PNP card structure\n");
#endif
		return -ENOMEM;
	}
	pnp_dev = kzalloc(sizeof(struct pnp_dev), GFP_KERNEL);
	if ( !pnp_dev )
	{
#ifdef CONFIG_PNP_DEBUG
		printk(KERN_ERR "Could not allocated PNP device structure\n");
#endif
		goto out_free_card;
	}
	dev_id  = kzalloc(sizeof(struct pnp_id), GFP_KERNEL);
	if ( !dev_id )
	{
#ifdef CONFIG_PNP_DEBUG
		printk(KERN_ERR "Could not allocated PNP device id structure\n");
#endif
		goto out_free_pnpdev;
	}
	dev_id2  = kzalloc(sizeof(struct pnp_id), GFP_KERNEL);
	if ( !dev_id2 )
	{
#ifdef CONFIG_PNP_DEBUG
		printk(KERN_ERR "Could not allocated PNP device id structure 2\n");
#endif
		goto out_free_devid;
	}

	card->number = mca_dev->slot;	// TODO: OK for now
	INIT_LIST_HEAD(&card->devices);
        card->protocol = &pnpmca_protocol;
	snprintf(dev_id2->id, PNP_ID_LEN, "MCA%04hX", mca_dev->pos_id);
	error = pnp_add_card_id(dev_id2, card);
	if ( error < 0 )
	{
#ifdef CONFIG_PNP_DEBUG
		printk(KERN_ERR "pnp_add_card_id failed: %d\n", error);
#endif
		goto out_free_devid2;
	}

	pnp_dev->number = 1;
	pnp_dev->data   = mca_dev;
	pnp_dev->card = card;
	snprintf(dev_id->id, PNP_ID_LEN, "MCA%04hX", mca_dev->pos_id);
	error = pnp_add_id(dev_id, pnp_dev);
	if ( error < 0 )
	{
#ifdef CONFIG_PNP_DEBUG
		printk(KERN_ERR "pnp_add_id failed: %d\n", error);
#endif
		goto out_free_devid2;
	}

	pnp_dev->capabilities |= PNP_CONFIGURABLE | PNP_DISABLE | PNP_READ | PNP_WRITE;
	pnp_dev->protocol = &pnpmca_protocol;
	pnp_init_resource_table(&pnp_dev->res);
printk(KERN_INFO "PNPMCA adding device MCA%04hX\n", mca_dev->pos_id);

	/*
	 * TODO: Free resource and device list when leaving after here
	 */

	error = mca_parse_resources(pnp_dev, mca_dev->pos_id);
	if ( error < 0 )
	{
		error = 0;	// Ignore it, else scanning stops
		goto out;
	}

	error = pnp_add_card_device(card, pnp_dev);
	if ( error < 0 )
	{
#ifdef CONFIG_PNP_DEBUG
		printk(KERN_ERR "pnp_add_card_device failed: %d\n", error);
#endif
		goto out;
	}

        error = pnp_add_card(card);
	if ( error < 0 )
	{
#ifdef CONFIG_PNP_DEBUG
		printk(KERN_ERR "pnp_add_card failed: %d\n", error);
#endif
		goto out;
	}

	return 0;

out:
	/*
	 * TODO: Free resource and device list when leaving here
	 */
	;
out_free_devid2:
	kfree(dev_id2);
out_free_devid:
	kfree(dev_id);
out_free_pnpdev:
	kfree(pnp_dev);
out_free_card:
	kfree(card);
	return error;
}

static int __init pnpmca_init(void)
{
	struct pnp_dev *dev;

	if ( pnp_register_protocol(&pnpmca_protocol) < 0 )
	{
		printk(KERN_ERR "Failed to register PNPMCA protocol\n");
		return -EBUSY;
	}

	bus_for_each_dev(&mca_bus_type, NULL, NULL, pnpmca_add_device_callback);

	protocol_for_each_dev(&pnpmca_protocol, dev) {
		printk(KERN_INFO "mcapnp:   Device '%s'\n",
				dev->name[0] ? dev->name : "Unknown");
        }

	return 0;
}

device_initcall(pnpmca_init);
