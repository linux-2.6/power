#ifndef _ASM_POWERPC_ASM_COMPAT_H
#define _ASM_POWERPC_ASM_COMPAT_H

#include <asm/types.h>

#ifdef __ASSEMBLY__
#  define stringify_in_c(...)	__VA_ARGS__
#  define ASM_CONST(x)		x
#else
/* This version of stringify will deal with commas... */
#  define __stringify_in_c(...)	#__VA_ARGS__
#  define stringify_in_c(...)	__stringify_in_c(__VA_ARGS__) " "
#  define __ASM_CONST(x)	x##UL
#  define ASM_CONST(x)		__ASM_CONST(x)
#endif


/*
 * Feature section common macros
 *
 * Note that the entries now contain offsets between the table entry
 * and the code rather than absolute code pointers in order to be
 * useable with the vdso shared library. There is also an assumption
 * that values will be negative, that is, the fixup table has to be
 * located after the code it fixes up.
 */
/* 32 bits kernel, 32 bits code */
#define MAKE_FTR_SECTION_ENTRY(msk, val, label, sect)	\
99:						       	\
	.section sect,"a";			       	\
	.align 2;				       	\
98:						       	\
	.long msk;				       	\
	.long val;				       	\
	.long label##b-98b;			       	\
	.long 99b-98b;				       	\
	.previous

/* operations for longs and pointers */
#define PPC_LL		stringify_in_c(l)
#define PPC_STL		stringify_in_c(st)
#define PPC_LCMPI	stringify_in_c(cmpwi)
#define PPC_LONG	stringify_in_c(.long)
#define PPC_TLNEI	stringify_in_c(tnei)
//define PPC_LLARX	stringify_in_c(lwarx)
//define PPC_STLCX	stringify_in_c(stwcx.)
#define PPC_CNTLZL	stringify_in_c(cntlz)
#define PPC_MTOCRF	stringify_in_c(mtcrf)

#endif /* _ASM_POWERPC_ASM_COMPAT_H */
