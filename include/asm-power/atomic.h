#ifndef _ASM_POWERPC_ATOMIC_H_
#define _ASM_POWERPC_ATOMIC_H_

typedef struct { volatile int counter; } atomic_t;

#ifdef __KERNEL__

#include <linux/compiler.h>
#include <asm/synch.h>
#include <asm/asm-compat.h>
#include <asm/system.h>

#define ATOMIC_INIT(i)          { (i) }

static __inline__ int atomic_read(const atomic_t *v)
{
        int t;

        __asm__ __volatile__("l%U1%X1 %0,%1" : "=r"(t) : "m"(v->counter));

        return t;
}

static __inline__ void atomic_set(atomic_t *v, int i)
{
        __asm__ __volatile__("st%U0%X0 %1,%0" : "=m"(v->counter) : "r"(i));
}

static __inline__ int __atomic_add_return(int i, atomic_t *v)
{
        int ret;
        unsigned long flags;
        _atomic_spin_lock_irqsave(v, flags);

        ret = (v->counter += i);

        _atomic_spin_unlock_irqrestore(v, flags);
        return ret;
}

static inline int atomic_cmpxchg(atomic_t *v, int old, int new)
{
        unsigned long flags;
        int prev;

        local_irq_save(flags);
        prev = atomic_read(v);
        if (prev == old)
                atomic_set(v, new);
        local_irq_restore(flags);
        return prev;
}

static inline int atomic_xchg(atomic_t *v, int new)
{
        unsigned long flags;
        int prev;

        local_irq_save(flags);
        prev = atomic_read(v);
        atomic_set(v, new);
        local_irq_restore(flags);
        return prev;
}

static __inline__ int atomic_add_unless(atomic_t *v, int a, int u)
{
        int c, old;
        c = atomic_read(v);
        for (;;) {
                if (unlikely(c == (u)))
                        break;
                old = atomic_cmpxchg((v), c, c + (a));
                if (likely(old == c))
                        break;
                c = old;
        }
        return c != (u);
}

#define atomic_inc_not_zero(v) atomic_add_unless((v), 1, 0)

#define atomic_add(i,v) ((void)(__atomic_add_return( ((int)i),(v))))
#define atomic_sub(i,v) ((void)(__atomic_add_return(-((int)i),(v))))
#define atomic_inc(v)   ((void)(__atomic_add_return(   1,(v))))
#define atomic_dec(v)   ((void)(__atomic_add_return(  -1,(v))))

#define atomic_add_return(i,v)  (__atomic_add_return( ((int)i),(v)))
#define atomic_sub_return(i,v)  (__atomic_add_return(-((int)i),(v)))
#define atomic_inc_return(v)    (__atomic_add_return(   1,(v)))
#define atomic_dec_return(v)    (__atomic_add_return(  -1,(v)))

#define atomic_add_negative(a, v)       (atomic_add_return((a), (v)) < 0)

#define atomic_inc_and_test(v) (atomic_inc_return(v) == 0)

#define atomic_dec_and_test(v)  (atomic_dec_return(v) == 0)

#define atomic_sub_and_test(i,v)        (atomic_sub_return((i),(v)) == 0)

/*
 *  * Atomically test *v and decrement if it is greater than 0.
 *   * The function returns the old value of *v minus 1, even if
 *    * the atomic variable, v, was not decremented.
 *     */
static __inline__ int atomic_dec_if_positive(atomic_t *v)
{
	int count;

	do
	{
		count = atomic_read(v);
	} while ( count > 0 && atomic_cmpxchg(v, count, count-1) != count );

	return count-1;
}

#define smp_mb__before_atomic_dec()     smp_mb()
#define smp_mb__after_atomic_dec()      smp_mb()
#define smp_mb__before_atomic_inc()     smp_mb()
#define smp_mb__after_atomic_inc()      smp_mb()



#include <asm-generic/atomic.h>
#endif /* __KERNEL__ */
#endif /* _ASM_POWERPC_ATOMIC_H_ */
