/*
 * Non-machine dependent bootinfo structure.  Basic idea
 * borrowed from the m68k.
 *
 * Copyright (C) 1999 Cort Dougan <cort@ppc.kernel.org>
 */

#ifdef __KERNEL__
#ifndef _PPC_BOOTINFO_H
#define _PPC_BOOTINFO_H

#include <asm/page.h>

extern unsigned long boot_mem_size;

#endif /* _PPC_BOOTINFO_H */
#endif /* __KERNEL__ */
