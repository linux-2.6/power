#ifndef _ASM_POWERPC_CACHE_H
#define _ASM_POWERPC_CACHE_H

#ifdef __KERNEL__


/* bytes per L1 cache line */
#define L1_CACHE_SHIFT		5
#define MAX_COPY_PREFETCH	4

#define	L1_CACHE_BYTES		(1 << L1_CACHE_SHIFT)

#define	SMP_CACHE_BYTES		L1_CACHE_BYTES

#if !defined(__ASSEMBLY__)
#define __read_mostly __attribute__((__section__(".data.read_mostly")))
#endif

#endif /* __KERNEL__ */
#endif /* _ASM_POWERPC_CACHE_H */
