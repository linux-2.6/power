/*
 * PowerPC memory management structures
 */

#ifdef __KERNEL__
#ifndef _PPC_MMU_H_
#define _PPC_MMU_H_


#ifndef __ASSEMBLY__

/*
 * Define physical address type.  Machines using split size
 * virtual/physical addressing like 32-bit virtual / 36-bit
 * physical need a larger than native word size type. -Matt
 */
#ifndef CONFIG_PHYS_64BIT
typedef unsigned long phys_addr_t;
#define PHYS_FMT	"%.8lx"
#else
typedef unsigned long long phys_addr_t;
extern phys_addr_t fixup_bigphys_addr(phys_addr_t, phys_addr_t);
#define PHYS_FMT	"%16Lx"
#endif

typedef struct {
	unsigned long id;
	unsigned long vdso_base;
} mm_context_t;

typedef struct _PFT {
	unsigned long i:1;	/* Invalid bit */
	unsigned long :11;	/* unused */
	unsigned long next:20;	/* index of next PFT */
} PFT;

/* Hardware Page Table Entry */
typedef struct _PTE {
	unsigned long vsid:24;	/* Virtual segment identifier */
	unsigned long vpi:3;	/* 3 high bits of VPI */
	unsigned long v:1;	/* Entry is valid */
	unsigned long r:1;	/* Referenced */
	unsigned long c:1;	/* Changed */
	unsigned long pp:2;	/* Page protection */
	PFT	      pft;	/* Next entry reference */
	unsigned long lockbits;
	unsigned long l:1;	/* lock type */
	unsigned long wl:1;	/* grant write locks */
	unsigned long rl:1;	/* grant read locks */
	unsigned long ra:1;	/* allow read */
	unsigned long :12;	/* unused */
	unsigned long tid:16;	/* tid */
} PTE;

/* Values for PP (assumes Ks=0, Kp=1) */
#define PP_RWXX	0	/* Supervisor read/write, User none */
#define PP_RWRX 1	/* Supervisor read/write, User read */
#define PP_RWRW 2	/* Supervisor read/write, User read/write */
#define PP_RXRX 3	/* Supervisor read,       User read */

/* Segment Register */
typedef struct _SEGREG {
	unsigned long t:1;	/* Normal or I/O  type */
	unsigned long ks:1;	/* Supervisor 'key' (normally 0) */
	unsigned long kp:1;	/* User 'key' (normally 1) */
	unsigned long n:1;	/* No-execute */
	unsigned long :4;	/* Unused */
	unsigned long vsid:24;	/* Virtual Segment Identifier */
} SEGREG;

#endif /* __ASSEMBLY__ */

#endif /* _PPC_MMU_H_ */
#endif /* __KERNEL__ */
