#ifndef _ASM_POWERPC_SYNCH_H 
#define _ASM_POWERPC_SYNCH_H 
#ifdef __KERNEL__

#include <linux/stringify.h>

#    define LWSYNC	dcs

#define ISYNC_ON_SMP
#define LWSYNC_ON_SMP

static inline void eieio(void)
{
	__asm__ __volatile__ ("dcs" : : : "memory");
}

static inline void isync(void)
{
	__asm__ __volatile__ ("ics" : : : "memory");
}

#endif /* __KERNEL__ */
#endif	/* _ASM_POWERPC_SYNCH_H */
